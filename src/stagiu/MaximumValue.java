package stagiu;

import java.util.Scanner;
import java.util.TreeSet;

public class MaximumValue {

	public static void main(String[] args) {
		
		Scanner in= new Scanner(System.in);
		int control,number,max=0;
		
		do {
			System.out.println("0 - Iesire program");
			System.out.println("1 - Aflare valoare maxima fara colectii");
			System.out.println("2 - Aflare valoare maxima folosind colectii");
			
			control=in.nextInt();
			
			number=in.nextInt();
			switch(control) {
			case 1:
				max=0;
				while(number!=0) {
					int c=number%10;
					if(c>max) {
						max=c;
					}
					number/=10;	
				}
				break;
			case 2:
				TreeSet<Integer> set = new TreeSet<Integer>();
				while(number!=0) {
					set.add(number%10);
					number/=10;
				}
				max=set.last();
				break;
			default:
				break;
				
			}
					
			System.out.println("Valoarea maxima: "+ max);
			
		}while(control!=0);
				
		in.close();
	}

}
